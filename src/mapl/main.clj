;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.main
  (:require [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as log]
            [mapl.lang.configuration :as cfg]
            [mapl.lang.agent :as agent]
            [mapl.lang.mas :as mas]
            [mapl.lang.print :as p]
            [environ.core :refer [env]])
  (:gen-class))

(def version (:mapl-version env))

(def cli-options
  [["-v" "--verbosity LEVEL" "Verbosity level"
    :id :verbosity
    :default :info
    :parse-fn keyword
    :validate [#(#{:trace :debug :info :warn :error :fatal :report} %)
               "Refer to https://github.com/ptaoussanis/timbre for logging levels"]]
   ["-p" "--print TYPE" "Print part of the config"
    :id :printing
    :default nil
    :parse-fn #(clojure.string/split % #",")
    :validate [#(every? (fn [s] (#{"ais" "pis" "env"} s)) %) 
               "Choose a combination of 'ais', 'pis', or 'env' and separate with commas"]]
   ["-c" "--cycles N" "Set a limit on number of cycles to execute"
    :id :cycles
    :default nil
    :parse-fn #(Integer/parseInt %)]])

(defn printing [switches m n]
  (doseq [switch switches]
    (condp = switch
      "ais"
      (doseq [a (mas/agents m)]
        (println (p/print-ais a)))

      "pis"
      (doseq [a (mas/agents m)]
        (println (p/print-pis a)))

      "env"
      (println
        (p/str-env (mas/environment m)))

      "delta"
      (p/print-manifests (mas/agents m))

      (when switch
        (println "")))))


(def help-banner
  (str
    "Meta-APL interpreter v" version "\n"
    "----------------------" (apply str
                                    (take (count version) (repeat "-")))
    "\n\n"

    "Command-line options are as follows:

    java -jar mapl-" version "-standalone.jar [options] path

                             The path should point to a file containing a MAS description in the EDN format given in the docs.

                             *  -p or --print (env | ais | pis | delta)*. Pass env to print the environment each cycle, ais to print the mental state of agents each cycle, pis to print the plan state of agents each cycle, delta to print a change log for each agent's configuration. The default is no printing, except the environment of the final cycle to be executed. Specify multiple print options as a comma separated list eg. env,ais,delta

                             *  -c or --cycle N. Pass an integer specifying the cycle number to reach before halting. The default is to continue indefinitely.

                             *  -v or --verbosity LEVEL. Specify the level of verbosity for logging, either trace, debug, error, warn, or info (see here for more details."))

(defn loop-for-n [m n opts]
  (let [trace (mas/deterministic-trace m)]
    (when (not (empty? (:printing opts)))
      (println "Initial state")
      (printing (:printing opts) (first trace) 0))
    (loop [trace (rest trace)
           cycles 0]
      (if (and (not (nil? n)) (>= cycles n))
        ;(pp/pprint (:environment (first trace)))
        (println (p/str-env (:environment (first trace))))
        (let [t (first trace)]
          (do
            (when (not (empty? (:printing opts)))
              (println "\nCycle " (str cycles) ":"))
            (printing (:printing opts) t cycles)
            (recur (rest trace) (inc cycles))))))))

(defn -main [& args]
  (let [cli (parse-opts args cli-options)
        opts (:options cli)
        args (:arguments cli)]
    (if (empty? args)
      (println help-banner)
      (do
        (log/set-level! (:verbosity opts))
        (let [m (mas/load-mas (slurp (first args)))
              trace (mas/deterministic-trace m)
              cycles (:cycles opts)]
          (loop-for-n m cycles opts))))))
