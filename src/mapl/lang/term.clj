;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.term)

; named 'term' for historical reasons
; actually just a collection of datatypes below the 'configuration' level
; and functions for formatting

;(defrecord LVar [name id]
;  Object
;  (toString [_]
;    (str name id)))

(defrecord ASTVar [name])

(defn ast-var [x]
  (->ASTVar x))


(defn ast-var? [x]
  (instance? ASTVar x))

;(defn fresh-lvar [x]
;  (->LVar (str x) (fresh)))

;(defn lvar 
;  ([name]
;   (->LVar (str name) nil))
;  ([name id]
;   (->LVar (str name) id)))

(defn lvar
  ([name]
   (keyword (str "lv|" name)))
  ([name id]
   (keyword (str "lv|" name "|" id))))

(defn lvar-vec [lv]
  ; [name id]
  (vec (rest
         (clojure.string/split (name lv) #"\|"))))

(defn lvar-name [lv]
  (first
    (lvar-vec lv)))

(defn lvar-id [lv]
  (second
    (lvar-vec lv)))

;(defn lvar? [x]
;  (instance? LVar x))

(defn lvar? [x]
  (and (keyword? x)
       (= (take 3 (name x))
          (seq "lv|"))))

; this is a shame

(defrecord Wildcard [])

(defn wildcard? [x]
  (instance? Wildcard x))

(def wildcard (->Wildcard))

(defmethod print-method Wildcard
  [o w]
  (print-simple
    (str "_")
    w))

;(defmethod print-method LVar
;  [o w]
;  (print-simple
;    (str "<" (.name o) (.id o) ">")
;    w))

(defn id [i]
  (keyword (str "id|" i)))

(defn str-id [id]
  (apply str (drop 3 (name id))))

(defn id? [x]
  (when (keyword? x)
    (=
     (first (clojure.string/split (name x) #"\|"))
     "id")))

(defn pred [[p & args]]
  p)

(defn args [[_ & args]]
  args)

(defn atom? [x]
  (and (sequential? x)
       (or (lvar? (first x))
           (string? (first x)))))

(defn -atom
  ( [p args]
   (vec
     (cons p args)))
  ([p]
   (-atom p nil)))

(declare str-atom)

(defn str-term [t]
  (cond
    (atom? t)
    (str-atom t)

    (wildcard? t)
    t

    (id? t)
    (str-id t)

    :else
    (str t)))

(defn str-atom [a]
  (let [p (pred a)
        as (args a)]
    (str p
         (when (not (empty? as))
           (str
             "(" (apply str 
                        (interpose ", " 
                                   (map str-term as)))
             ")")))))

(defn htlist [h t]
  (vector
    :htlist
    h
    t))

(defn head [[_ h t]]
  h)

(defn tail [[_ h t]]
  t)

(defn ht->list [ht]
  (vec
    (cons (head ht)
          (tail ht))))

(defn htlist? [x]
  (and (sequential? x)
       (not (empty? x))
       (= (first x) :htlist)))

(defn -list? [x]
  (and (sequential? x)
       (not (htlist? x))))

(defn arith-expr [op l r]
  (vector op l r))

(def arith-ops
  {:+ +
   :- -
   :* *
   :/ /
   :div quot
   :mod mod})

(defn arith-expr? [x]
  (and (sequential? x)
       (= 3 (count x))
       (contains? arith-ops (first x))))

(defn str-arith [x]
  (cond
    (number? x)
    (str x)

    (lvar? x)
    (str x)

    (arith-expr? x)
    (let [[op l r] x]
      (str "(" (str-arith l) " " (name op) " " (str-arith r) ")"))))

(defn rel-expr [op l r]
  (vector op l r))

;TODO unification operator

(def rel-ops
  {:> >
   :< <
   :>= >=
   :<= <=
   :== ==})

(defn rel-expr? [x]
  (and (sequential? x)
       (= 3 (count x))
       (contains? rel-ops (first x))))

(defn str-logexpr- [ex]
  (cond
    (= (first ex) :not)
    (str "not " (str-logexpr- (second ex)))

    (= (count ex) 3)
    (let [[op l r] ex]
      (case op
        :and (str
               "("
               (str-logexpr- l)
               ", "
               (str-logexpr- r)
               ")")
        :or (str
              "("
              (str-logexpr- l)
              " | "
              (str-logexpr- r)
              ")")))

    :else
    ; (let [_ (assert (= (count ex) 1) (str ex))]
    (str-atom ex)))

(defn str-logexpr [ex]
  (let [s (str-logexpr- ex)]
    (if (and
          (= (first s) \()
          (= (last s) \)))
      (subs s 1 (dec (count s)))
      s)))

; action types
(defn query-action [expr]
  {:type :query
   :value expr})

(defn subgoal-action [a]
  {:type :subgoal
   :value a})

(defn atomform-action [a]
  {:type :atomform
   :value a})

(def act-value :value)

(defn query-action? [act]
  (= (:type act) :query))

(defn subgoal-action? [act]
  (= (:type act) :subgoal))

(defn atomform-action? [act]
  (= (:type act) :atomform))

(defn action? [x]
  (or (query-action? x)
      (subgoal-action? x)
      (atomform-action? x)))

(defn plan-body? [p]
  (every? action? p))
