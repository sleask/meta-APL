;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.primitive-action
  (:require [mapl.lang.configuration :as cfg]
            [mapl.lang.term :refer :all]
            [mapl.lang.unify :refer [unify]]
            [mapl.lang.eval :refer [-eval]]
            [cats.core :as m]
            [cats.monad.maybe :as maybe]
            [cats.context :refer [with-context]]))

(defn make-state [config s]
  [config s])

(defn update-config [state f]
  (update-in state [0] f))

(defn update-substitution [state f]
  (update-in state [1] f))

(def config first)
(def substitution second)

(defn ground [state v]
  (if-let [v' (-eval v (substitution state))]
    (maybe/just v')
    (maybe/nothing)))


; 'return' the id by binding with 'last-created-id'
(defn add-atom-2 [i a]
  (fn [state]
    (let [c (cfg/add-ai (config state) (vec
                                         (-eval a (substitution state))) #{})]
      (if-let [s' (unify i (cfg/last-id-created c) (substitution state))]
        (maybe/just (make-state c s'))
        (maybe/nothing)))))

(defn add-atom-1 [a]
  (add-atom-2 wildcard a))

; TODO
; -eval doesn't throw an error if it's unbound
; it definitely should, or else propagate nils up to the top
; a single unbound var should make it insta-nil
; ignore for time being


(declare del-plan)

(defn del-dependent-plans [i]
  (fn [state]
    (with-context maybe/context
      (m/mlet [i (m/return (-eval i (substitution state)))

               plan-ids (m/return
                          (cfg/justified-plans (config state) i))

               state (m/foldm
                       (fn [maybe-state id]
                         ((del-plan id) maybe-state))
                       state
                       (seq plan-ids))

               state (m/return
                       (update-config state #(cfg/del-just-relations % i)))]

              (m/return state)))))


(defn del-atom [i]
  (fn [state]
    (with-context maybe/context
      (m/mlet [i (m/return (-eval i (substitution state)))

               state ((del-dependent-plans i) state)

               state (m/return
                       (update-config state #(cfg/del-subgoal-relation % i)))

               state (m/return (update-config state #(cfg/del-ai % i)))]

              (m/return state)))))

(defn del-subgoals [i]
  (fn [state]
    (with-context maybe/context
      (m/mlet [ids (m/return (cfg/child-goals (config state) i))
               state (m/foldm
                       (fn [state i]
                         ((del-atom i) state))
                       state
                       ids)]

              (m/return (update-config state #(cfg/del-plan-subgoals % i)))))))


(defn del-plan [i]
  (fn [state]
    (with-context maybe/context
      (m/mlet [i (m/return
                   (-eval i (substitution state)))

               state ((del-subgoals i) state)

               ; delete justification-relation too
               state (m/return
                       (update-config state #(cfg/del-plan-just-relations % i)))

               state (m/return
                       (update-config state #(cfg/del-pi % i)))]

              (m/return state)))))


(defn set-pi-field [access]
  (fn [i v]
    (fn [state]
      (with-context maybe/context
        (m/mlet [i (m/return (-eval i (substitution state)))
                 v (m/return (-eval v (substitution state)))
                 :when (cfg/plan-instance (config state) i)
                 state (m/return
                         (update-config state
                                        (fn [config]
                                          (cfg/update-pi config i
                                                         #(assoc % access v)))))]
                (m/return state))))))


;(def set-state (set-pi-field cfg/state))

(defn set-state [i s]
  (fn [state]
    (with-context maybe/context
      (m/mlet [i (ground state i)
               s (ground state s)
               :when (cfg/plan-instance (config state) i)
               state (m/return
                       (update-config state
                                      (fn [config]
                                        (cfg/update-pi config i
                                                       #(assoc % cfg/state (set s))))))]
              (m/return state)))))

(defn set-substitution [i s]
  (fn [state]
    (with-context maybe/context
      (m/mlet [i (ground state i)
               s (ground state s)
               :when (cfg/plan-instance (config state) i)
               state (m/return 
                       (update-config state
                                      (fn [config]
                                        (cfg/update-pi config i
                                                       #(assoc % cfg/substitution
                                                               (if (or (vector? s) (set? s))
                                                                   (into {} s)
                                                                 s))))))]
              (m/return state)))))

(defn set-remainder [i p]
  (fn [state]
    (with-context maybe/context
      (m/mlet [i (ground state i)
               p (ground state p)
               :when (cfg/plan-instance (config state) i)
               state (m/return
                       (update-config state
                                      (fn [config]
                                        (cfg/update-pi config i
                                                       #(assoc % cfg/remainder p)))))]
               (m/return state)))))


(defn nothing []
  (fn [state]
    (maybe/just state)))


(def primitive-actions
  {"add-atom|2" add-atom-2
   "add-atom|1" add-atom-1
   "delete-atom|1" del-atom
   "delete-plan|1" del-plan
   "set-substitution|2" set-substitution
   "set-state|2" set-state
   "set-remainder|2" set-remainder
   "nothing|0" nothing})

(defn atom->key [a]
  (str (pred a) "|" (count (args a))))

(defn primitive-action? [a]
  (contains? primitive-actions (atom->key a)))

(defn atom->action [a s]
  (let [k (atom->key (-eval a s))]
    (apply
      (get primitive-actions k)
      (args a))))

(defn primitive-action [a config s]
  (let [action (atom->action a s)]
    (maybe/from-maybe
      (action (make-state config s)))))
