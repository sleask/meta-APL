;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.execution
  (:require [mapl.lang.configuration :as cfg]
            [mapl.lang.action :as action]
            [mapl.lang.term :refer [-atom]]))

(def scheduled (-atom "scheduled"))
(def stepped (-atom "stepped"))
(def failed (-atom "failed"))

(defn scheduled? [pi]
  (contains? (cfg/state pi) scheduled))

(defn failed? [pi]
  (contains? (cfg/state pi) failed))

(defn executable-instances [config]
  (filter #(and (scheduled? %) (not (failed? %))) (cfg/plan-instances config)))

(defn successful-step [state planid]
  (update-in state [action/config]
             #(-> %
                  (cfg/add-flag planid stepped)
                  (cfg/remove-flag planid scheduled)
                  (update-in [:plan-instances planid cfg/remainder] rest)
                  (assoc-in [:plan-instances planid cfg/substitution] (action/substitution state)))))

(defn failed-step [state planid]
  (update-in state [action/config]
             #(-> %
                  ;(cfg/add-flag planid stepped)
                  (cfg/remove-flag planid scheduled)
                  (cfg/add-flag planid failed))))

(defn plan-action [ast-act config nspace planid env n] 
  ; TODO logging
  (let [_ (assert (not (nil? ast-act)))
        act (action/convert-AST-plan-act ast-act nspace planid)
        s (get-in config [:plan-instances planid cfg/substitution])]
    (act {action/substitution s
          action/config config
          action/environment env
          :name n}
         #(successful-step % planid)
         #(let [msg (first %)
                state (second %)
                _ (assert (some? state))]
            ; logging!
            (failed-step state planid)))))


(defn step-instance [pi config nspace env n]
  ; assume executable
  ; if remainder is empty, do we flag as failed?
  (let [planid (cfg/id pi)
        remainder (get-in config [:plan-instances planid cfg/remainder])
        _ (assert (not (empty? remainder)) (str "trying to execute the empty remainder of plan with id: " planid))]
    (plan-action (first remainder) config nspace planid env n)))

(defn step-instances [config nspace env n]
 ; (map #(vector (action/config %)
  ;              (action/environment %))
       (reduce
         (fn [state pi]
           (step-instance pi (action/config state) nspace (action/environment state) n))
         {action/config config
          action/environment env
          :name n}
         (executable-instances config)))

