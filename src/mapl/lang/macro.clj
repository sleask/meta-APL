;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.macro
  (:require [mapl.lang.term :refer :all]))

(defn head->key [h]
  (keyword (str (pred h) "|" (count (args h)))))

(defn add-binding [nspace head body]
  {:pre [(atom? head)]}
  (let [k (head->key head)]
    (update-in nspace [:macros k] #(vec (conj (vec %) [head body])))))

(defn by-atom [nspace a]
  (get-in nspace [:macros (head->key a)]))

(defn exists? [nspace a]
  (contains? (:macros nspace) (head->key a)))


