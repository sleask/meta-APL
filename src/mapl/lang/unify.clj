;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.unify
  (:require [clojure.walk :refer [postwalk]]
            [mapl.lang.eval :refer [-eval]]
            [mapl.lang.term :refer :all]))

(def counter (atom -1))

(defn fresh []
  (swap! counter inc))

(def empty-substitution {})

(defn prep-term 
  ([t id]
    (postwalk
      (fn [form]
        (if (ast-var? form)
          (lvar (:name form) id)
          form))
      t))
  ([t]
   (let [id (fresh)]
     (prep-term t id))))

(defn prep-terms
  ([ts id]
   (map #(prep-term % id) ts))
  ([ts]
   (prep-terms ts (fresh))))


(def fail false)
(def fail? false?)


(declare unify)

(defn unify-action [act y s]
  (cond 
    (query-action? act)
    (unify (act-value act) y s)

    (subgoal-action? act)
    (unify (act-value act) y s)

    (atomform-action? act)
    (unify (act-value act) y s)

    :else
    (throw (Exception. (str "this isn't an action: " act))))) 

(defn unify-two-actions [act1 act2 s]
  "when unifying two actions, type must be the same"
  (if (= (:type act1) (:type act2))
    (unify (act-value act1) (act-value act2) s)
    fail))

(defn unify [x y s]
  (cond
    (fail? s)
    fail

    (or (wildcard? x) (wildcard? y))
    s

    (arith-expr? x)
    (unify (-eval x s) y s)

    (arith-expr? y)
    (unify x (-eval y s) s)

    (lvar? x)
    (if-let [resolved (get s x)]
      (recur resolved y s)
      (assoc s x y))

    (lvar? y)
    (if-let [resolved (get s y)]
      (recur x resolved s)
      (assoc s y x))


    (and (-list? x) (-list? y))
    (cond 
      (and (seq x) (seq y))
      (unify (rest x) (rest y)
             (unify (first x) (first y) s))

      (and (empty? x) (empty? y))
      s

      :else
      fail)

    (htlist? x)
    (if (-list? y)
      (unify (tail x) (rest y)
        (unify (head x) (first y) s))
      
      (unify (tail x) (tail y)
             (unify (head x) (head y) s)))

    (htlist? y)
    (unify y x s)

    (and (atom? x) (atom? y))
    (unify (args x) (args y)
           (unify (pred x) (pred y) s))

    (and (action? x)
         (action? y))
    (unify-two-actions x y s)

    (action? x)
    (unify-action x y s)

    (action? y)
    (unify-action y x s)

    (set? x)
    (unify (into [] x) y s)

    (set? y)
    (unify x (into [] y) s)

    (= x y)
    s

    :else
    fail))


