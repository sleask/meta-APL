;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.agent
  (:require [mapl.lang.configuration :as cfg]
            [mapl.lang.rule :as rule]
            [mapl.lang.execution :refer :all]
            [mapl.lang.macro :as macro]
            [mapl.lang.clause :as clause]
            [mapl.lang.parser :as parse])
  (:refer-clojure :exclude [name namespace cycle agent]))

(def name :name)
(def id :id)
(def source-file :source-file)
(def sensefn :sensefn)
(def namespace :namespace)
(def config :config)
(def rulesets :rulesets)

(defn agent
  ([n source -sensefn -namespace -config -rulesets]
   {id (keyword (gensym n))
    name n
    source-file source
    sensefn -sensefn
    namespace -namespace
    config -config
    rulesets -rulesets}))

(def blank-namespace
  {:clauses {}
   :macros {}})


(defn label-object-ruleset [rset id]
  (let [labelled-rules
        (map
          (fn [r n]
            (assoc r rule/id [id n]))
          (rule/ruleset-rules rset)
          (range))]
    (assoc rset rule/ruleset-rules labelled-rules)))


(defn label-rulesets-with-ids [rsets]
  ; rulesets need an integer id, rules need an a tuple [i j]
  (map
    (fn [rset n]
      (if (rule/object-ruleset? rset)
        (-> rset
            (assoc rule/id n)
            (label-object-ruleset n))
        rset))
    rsets
    (range)))


(defn pieces->agent [m]
  (let [nspace (reduce
                 (fn [n [h b]]
                   (clause/add-binding n h b))
                 blank-namespace
                 (:clauses m))

        nspace (reduce
                 (fn [n [h b]]
                   (macro/add-binding n h b))
                 nspace
                 (:macros m))

        config (reduce
                 (fn [c g]
                   (cfg/add-ai c g #{cfg/top-level cfg/goal}))
                 cfg/blank-config
                 (:goals m))

        config (reduce
                 (fn [c a]
                   (cfg/add-ai c a #{cfg/top-level}))
                   config 
                   (:atoms m))]
    (agent nil nil nil nspace config (label-rulesets-with-ids (:rulesets m)))))


(defn parse-agent [s]
  (-> s 
      (parse/parse)
      (parse/collect-pieces)
      (pieces->agent)))

(defn parse-agent-file [s]
  (->
    (slurp s)
    (parse-agent)
    (assoc source-file s)))

;(defn add-percept [ag a]
;  (update-in ag [:config] #(cfg/add-ai % a #{cfg/percept})))
;
;(defn add-percepts [ag as]
;  (reduce
;    add-percept
;    ag
;    as))

; simply add new percepts to the mental state
(defn default-belief-update [config new-percepts]
  (reduce
    (fn [c p]
      (cfg/add-ai c p #{cfg/percept}))
    config
    new-percepts))

(defn inc-phase [agent]
  (update-in agent [config cfg/phase] inc))

(defn reset-phase [agent]
  (assoc-in agent [config cfg/phase] 0))

(defn inc-cycle [agent]
  (update-in agent [config cfg/cycle] inc))

(defn reset-manifest [agent]
  (assoc-in agent [config cfg/manifest] cfg/empty-manifest))

; the sensefn field in agent record is agent->agent
; distinct from sense in environment which is env->percepts
; and the BUF, which is config+percepts -> config
; ideally sensefn incorporates both
(defn sense-phase [agent env]
  (let [sense (sensefn agent)]
    (-> agent
        (sense env)
        (inc-phase))))

; agent -> agents
(defn rule-phase [agent rs]
  (let [c (config agent)]
    (->>
      (rule/execute-ruleset rs c (namespace agent))
      (map #(assoc agent config %))
      (map inc-phase))))

(defn lazier-mapcat [f xs]
  (letfn [(lmapcat 
            [f q xs]
            (cond
              (some? (seq q))
              (lazy-seq
                (cons
                  (first q)
                  (lmapcat f (rest q) xs)))

              (empty? xs)
              []

              :else
              (let [ys (f (first xs))]
                (lmapcat f ys (rest xs)))))]

    (lmapcat f [] xs)))



(defn rule-phases [agent]
  (reduce
      (fn [agents rs]
        (lazier-mapcat #(rule-phase % rs) agents))
      [agent]
      (rulesets agent)))


(defn execution-phase [agent env]
  (let [state (step-instances (config agent) (namespace agent) env (name agent))]
    ; current assumption: execution phase is deterministic
    ; this will probably change
    (->> [[(config state) (:environment state)]]
         (map #(vector (assoc agent config (first %)) (second %)))
         (map #(update-in % [0] inc-phase)))))

; TODO phase comes out as one too high

(defn cycle [agent env]
 (let [agents
       (-> agent
           (reset-phase)
           (reset-manifest)
           (sense-phase env)
           (rule-phases))] 
        (->> agents
             (mapcat #(execution-phase % env))
             (map #(update-in % [0] inc-cycle)))))
; [agent,env] pairs

(defn deterministic-trace [agent env]
  (let [go (fn [[agent env]]
                (first (cycle agent env)))]
  (iterate go [agent env])))


