;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.manifest
  (:refer-clojure :exclude [cycle]))

; use to log changes so that logging is centralised and not a mess scattered throughout the codebase
; also should prevent logging from alternate timelines, which is very confusing to read

(defprotocol Event
  (to-string [this]))

(defrecord addition-event [datatype id data]
  Event
  (to-string [this]
    (str "(+) " datatype " added (id=" id "): " data)))

(defrecord deletion-event [datatype id data]
  Event 
  (to-string [this]
    (str "(-) " datatype " removed (id=" id "): " data)))

(defrecord increment-event [field new-value]
  Event
  (to-string [this]
    (str "(+1) " field " incremented to " new-value)))

(defrecord update-event [datatype id updated-field updated-value]
  Event
  (to-string [this]
    (str "(*) " datatype " with id " id " " updated-field " updated to " updated-value)))

(defrecord execution-event [action-type action successful? failure-reason] 
  Event
  (to-string [this]
    (if successful?
      (str "(->) executed " action-type ": " action)
      (str "(->!) executing " action-type ": " action " failed! " (if failure-reason
                                                                    (str
                                                                      "Reason: " failure-reason)
                                                                    "No reason given!")))))
