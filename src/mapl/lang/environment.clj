;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.environment
  (:refer-clojure :exclude [update]))

(defprotocol Environment
  (sense [env agent-name])
  (external-action [env agent-name a])
  (external-action? [env agent-name a])
  (belief-update [env config new-percepts])
  (update [env]) ; use to implement synchrony with MAS' cycle
  (observables [env])) ; return a datastructure representing only the 'observable' state of the environment, expect to be used to test equivalence

(defn success [env]
  {:tag :success :value env})

(defn failure [env msg]
  {:tag :failure :value env :reason msg})

(defn failed? [m]
  (= (:tag m) :failure))

(defn success? [m]
  (= (:tag m) :success))


