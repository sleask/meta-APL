;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.parser
  (:require [instaparse.core :as instaparse]
             [instaparse.failure :as fail]
             [mapl.lang.term :refer :all]
            [mapl.lang.rule :as rule])
  (:refer-clojure :exclude [atom]))



(def whitespace
  (instaparse/parser
    "whitespace = (sp | comment)*
    <sp> = <#'\\s+'>
    <comment> = <#'#(.*|\n)'>"))

(def agent-grammar
  "<agent> = piece+
  <piece> = clause | macro | (oruleset / mruleset) | (initial_goal / initial_belief)
  initial_belief = !('!') atomic_formula <'.'>
  initial_goal = <'!'> atomic_formula <'.'>
  macro = atomic_formula <'='> macro_body <'.'>
  clause = atomic_formula <'<-'> log_expr <'.'>
  oruleset = <'ruleset'> <ruleset_id>? <'{'> objectrule+ <'}'>
  mruleset = <'ruleset'> <ruleset_id>? <'{'> metarule+ <'}'>
  ruleset_id = #'[a-zA-Z0-9-_\\ ]*'
  objectrule = reasons [ <':'> log_expr ] <'->'> [ plan_body ] <'.'>
  metarule = log_expr <'->>'> metarule_body <'.'>
  reasons = atomic_formula ( <','> atomic_formula )*
  log_expr = nlog_expr
    | log_expr ',' log_expr
    | log_expr '|' log_expr
  nlog_expr = ('not' nlog_expr
    / <'('> log_expr <')'>
    / simple_log_expr)
  simple_log_expr = ( atomic_formula | rel_expr | var )
  metarule_body = atomic_formula_action ( <','> atomic_formula_action )*
  plan_body = [ plan_body_formula ] ( <';'> plan_body_formula )*
  macro_body = macro_action (<','> macro_action)*
  macro_action = (query_action | atomic_formula_action)
  plan_body_formula = (subgoal_action | query_action | atomic_formula_action)
  query_action = <'?'> nlog_expr
  subgoal_action = <'!'> atomic_formula
  atomic_formula_action = !('!'|'?') atomic_formula
  atomic_formula = ( ident | var ) [ <'('> arglist <')'> ]
  arglist = arg ( <','> arg)*
  <arg> = term | log_expr
  term = atomic_formula
    | list
    | arith_expr
    | var
    | '-' arith_simple
    | <'('> arith_expr <')'>
    | subgoal_action
    | query_action
  list = llist | htlist
  llist = <'['> term? (<','> term)* <']'>
  htlist = <'['> term <'|'> term <']'>
  rel_expr = rel_term ( '<' | '<=' | '>' | '>=' | '==' | '=' ) rel_term
  rel_term = ( atomic_formula | arith_expr )
  arith_expr = arith_term [ ('+' | '-' ) arith_expr ]
  arith_term = arith_simple [ ('*' | '/' | 'div' | 'mod') arith_term ]
  arith_simple = number
    | var
    | '-' arith_simple
    | <'('> arith_expr <')'>
  number = #'[0-9]+' [ #'[.][0-9]+' ]
  var = #'[A-Z][a-zA-Z0-9]*' | <#'_'>
  string = <q> #'[a-zA-Z0-9\\ ]*' <q>
  <q> = '\"'
  <nl> = #'\n'
  ident = #'[+a-zA-Z0-9-_]*'")

; htlist used to be this
 " htlist = <'['> term <'|'> term? (<','> term)* <']'>"

(def agent-parser
  (instaparse/parser agent-grammar :auto-whitespace whitespace))

(defn -lvar 
  ([v]
  (ast-var v))
  ([]
   wildcard))

(defn number
  ([i]
   (read-string i))
  ([i d]
   (read-string (clojure.string/join i d))))

(def ops
  {"+" :+
   "-" :-
   "*" :*
   "/" :/
   "mod" :mod
   "div" :div
   ">" :>
   "<" :<
   ">=" :>=
   "<=" :<=
   "=" :=
   "==" :==})

(defn -wildcard [_]
  wildcard)

(defn atom
  ([f]
   (-atom f []))
  ([f args]
   (-atom f args)))

(defn -list [& args]
  (if (some? args)
    (vec args)
    []))

(defn -htlist [h tail]
  (if (some? tail)
    ;(vec (cons h tail))
    (vector :htlist h tail)
    [h]))

(defn -arith-expr
  ([term op expr]
    (arith-expr (ops op) term expr))
  ([n]
   n))

(defn -rel-expr 
  ([l op r]
    (rel-expr (ops op) l r))
  ([n]
   n))

(defn nlog-expr
  ([n ex]
   [:not ex])
  ([ex]
   ex))

(defn log-expr
  ([nlog]
   nlog)
  ([l op r]
   (case op
     "," [:and l r]
     "|" [:or l r])))

;(defn object-rule
;  ([rs c body]
;   (rule/object-rule rs c body nil))
;  ([rs body]
;   (rule/object-rule rs nil body nil)))

; more hacks
(defn object-rule [rs & others]
  (condp = (count others)
    0
    (rule/object-rule rs nil [] nil)

    1
    (if (plan-body? (first others))
      (rule/object-rule rs nil (first others) nil)
      (rule/object-rule rs (first others) [] nil))

    2
    (if (plan-body? (first others))
      (rule/object-rule rs (second others) (first others) nil)
      (rule/object-rule rs (first others) (second others) nil))

    :else
    (throw (Exception. "too many pieces of object rule in here"))))

(defn meta-rule
  [c body]
  (rule/meta-rule c body))

(defn oruleset [& rules]
  (rule/object-ruleset rules nil))

(defn mruleset [& rules]
  (rule/meta-ruleset rules nil))

(defn macro [head body]
  [:macro head body])

(defn clause [head body]
  [:clause head body])

;(defn query-action [expr]
;  (let [_ (assert (some? expr))]
;    (action/query-action expr)))
;
;(defn subgoal-action [a]
;  (let [_ (assert (some? a))]
;    (action/subgoal-action a)))
;
;(defn atomform-action [a]
;  (let [_ (assert (some? a))]
;    (action/atomform-action a)))

(def transform-opts
  {:var -lvar
   :number number
   :arith_simple identity
   :string identity
   :ident identity
   :arith_term -arith-expr
   :arith_expr -arith-expr
   :rel_term -rel-expr
   :rel_expr -rel-expr
   :llist -list
   :htlist -htlist
   :list identity
   :term identity
   :atomic_formula atom
   :simple_log_expr identity
   :nlog_expr nlog-expr
   :log_expr log-expr
   :query_action query-action
   :subgoal_action subgoal-action
   :atomic_formula_action atomform-action
   :plan_body_formula identity ;action/atomform-action
   :plan_body vector
   :macro_action identity
   :macro_body vector
   :arglist vector
   :reasons vector
   :metarule meta-rule
   :metarule_body vector
   :objectrule object-rule
   :oruleset oruleset
   :mruleset mruleset
   :clause clause
   :macro macro})


(defn collect-pieces [parse-out]
  (reduce
    (fn [m elem]
      (cond
        (or (rule/object-ruleset? elem) (rule/meta-ruleset? elem))
        (update-in m [:rulesets] #(vec (conj % elem)))

        (= (first elem) :clause)
        (update-in m [:clauses] #(vec (conj % (rest elem))))

        (= (first elem) :macro)
        (update-in m [:macros] #(vec (conj % (rest elem))))

        (= (first elem) :initial_goal)
        (update-in m [:goals] #(vec (conj % (second elem))))

        (= (first elem) :initial_belief)
        (update-in m [:atoms] #(vec (conj % (second elem))))

        :else
        (throw (Exception. (str "unrecognised element: " elem)))))
    {:macros []
     :clauses []
     :rulesets []
     :goals []
     :atoms []}
    parse-out))

;(defn parse [s]
;    (instaparse/transform transform-opts (agent-parser s)))

(defn parse-opts [source & options]
  ; add a test for failure
  ; repropagate the exception if there is one
  (let [tree (apply agent-parser source options)]
    (if (instaparse/failure? tree)
      (throw (Exception. (with-out-str (fail/pprint-failure tree))))
      (instaparse/transform transform-opts tree))))

(defn parse [s]
  (parse-opts s))

(defn partial-parse [kw source]
  (parse-opts source :start kw))

(defn parse-atom [s]
  (partial-parse :atomic_formula s))


