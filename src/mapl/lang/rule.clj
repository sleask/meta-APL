;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.rule
  (:require [mapl.lang.unify :refer :all]
            [mapl.lang.logic :as l]
            [mapl.lang.term :refer [ast-var]]
            [taoensso.timbre :as log]
            [mapl.lang.action :as action]
            [mapl.lang.configuration :as cfg]))

(defrecord Metarule [context body])

(defn meta-rule? [x]
  (instance? Metarule x))

(defn meta-rule [context body]
  (->Metarule context body))

(defrecord Objectrule [reasons context body id])

(defn object-rule? [x]
  (instance? Objectrule x))

(defn object-rule [reasons context body id]
  (->Objectrule reasons context body id))

(def reasons :reasons)
(def context :context)
(def body :body)
(def id :id)

(defn prep-rule [r]
  (let [r (prep-term r)]
    {:rule r
     :substitution empty-substitution}))

(def rule :rule)
(def substitution :substitution)

(defn object-ruleset [rs id]
  {:id id
   :type :object
   :rules rs})

(defn meta-ruleset [rs id]
  {:id id
   :type :meta
   :rules rs})

(def ruleset-type :type)
(def ruleset-rules :rules)

(defn object-ruleset? [x]
  (and (= (:type x) :object)
       (contains? x :rules)))

(defn meta-ruleset? [x]
  (and (= (:type x) :meta)
       (contains? x :rules)))
;;;;

(defn check-context [cxt config nspace s]
  (let [substitutions
        (if (nil? cxt)
          (l/into-results s)
          (l/solve cxt config nspace s))]
    (when (seq substitutions)
      substitutions)))


(defn fireable-meta? [prepped-r config nspace]
  (let [substitutions
        (check-context (context (rule prepped-r))
                       config
                       nspace
                       (substitution prepped-r))]
    (when substitutions
      (map #(assoc prepped-r substitution %) substitutions))))


(defn apply-meta-body [config b s nspace]
  (let [b' (map #(action/convert-AST-metarule-act % nspace) b)

        [status result]
        ((action/macro-body b' nspace)
         {action/config config
          action/substitution s}
         #(vector :success %)
         #(vector :failure %))]
    (if (= :success status)
      (action/config result)
      (do
        (log/warn "meta-body failed")
        config))))

(defn apply-first-applicable-meta [config rules nspace]
  {:pre [(sequential? rules)]}
  ; {:post [(not (some #{config} %))]}
  (first 
    ;  (remove nil?
    (for [r rules
          :let [p-rule (prep-rule r)
                p-rules (fireable-meta? p-rule config nspace)]
          :when p-rules]
      ;  (do
      ;   (log/debug "firing rule" (str (id (:rule p-rule))))
      (do
        (log/debug "firing meta rule: " (str (context r)))
        (map 
          #(apply-meta-body config 
                            (body (:rule %)) 
                            (substitution %)
                            nspace)
          ; (id (:rule %))
          p-rules)))))


(defn execute-meta-ruleset-l [rules nspace frontier seen eq]
  (cond
    (empty? frontier)
    []

    (some #(eq (peek frontier) %) seen)
    (lazy-seq
      (execute-meta-ruleset-l
        rules
        nspace
        (pop frontier)
        seen
        eq))

    :else
    (let [config (peek frontier)
          nodes (apply-first-applicable-meta config rules nspace)
          remov (fn [elems sq]
                  (remove (fn [s]
                            (some #(eq s %) elems))
                          sq))]
      (if (not (empty? nodes))
        (lazy-seq
          (execute-meta-ruleset-l
            rules
            nspace
          ;  (into [] (concat (remov seen nodes) (pop frontier)))
            (into [] (concat (pop frontier) nodes))
            (conj seen config)
            eq))

        (lazy-seq
        (cons
          config
            (execute-meta-ruleset-l
              rules
              nspace
              (pop frontier)
              (conj seen config)
              eq)))))))


(defn execute-meta-ruleset [config rules nspace]
  (let [configs (execute-meta-ruleset-l rules nspace [config] [] (memoize cfg/config-eq))]
    (if (empty? configs)
      [config]
      configs)))


;;;;


; don't forget, justs are accumulated in the substitution now!
(defn match-on-reasons [config prepped-rule]
  (let [rs (reasons (:rule prepped-rule))
        match-rule (fn [prule reason] ; prule -> prule
                     (let [s (substitution prule)
                           ss (l/solve-atom reason config nil s true)]
                       (if (empty? ss)
                         []
                         (map #(assoc prule substitution %) ss))))]

    (distinct
      (reduce
        (fn [prules reason]
          (mapcat #(match-rule % reason) prules))
        [prepped-rule]
        rs))))

; should also check substitution?
(defn already-exists? [config matched-rule]
  (let [r (:rule matched-rule)
        i (id r)
        js (l/justs (substitution matched-rule))
        pis (cfg/plan-instances config)

        matching-instance? 
        (fn [pi]
          ;(let [[js' origin _] (cfg/extract-useful-from-pi pi config)]
          (let [js' (cfg/plan-justifications config (cfg/id pi))
                origin (cfg/originator pi)]
            (and (= (set js) (set js'))
                 (= i origin))))]

    (some matching-instance? pis)))

(defn fireable-object? [config unprepped-rule nspace]
  (let [firing-rules (for [r (match-on-reasons config (prep-rule unprepped-rule))
                           :let [valid-substitutions (->>
                                                       (check-context (context (:rule r)) config nspace (substitution r))
                                                       (remove nil?))]
                           :when (and
                                   (not (empty? valid-substitutions))
                                   (not (already-exists? config r)))]
                       (assoc r substitution (first valid-substitutions)))]
    (when (seq firing-rules)
      firing-rules)))

; should we be using the result of applying the context
; to introduce non-determinism?
(defn apply-first-applicable-object [config ruleset nspace]
  (first
    (for [r ruleset
          :let [;prepped-rule (prep-rule r)
                fireable-rules (fireable-object? config r nspace)]
          :when fireable-rules]
      (reduce
        (fn [c {s substitution
                {i id b body} rule}]
          (let [js (l/justs s)
                _ (assert (not (empty? js)))] ; must have justificatiions
            (as-> c
              c (cfg/add-pi c b #{} (dissoc s l/justs) i)
              c (cfg/add-justifications c (cfg/last-id-created c) js))))
        config
        fireable-rules))))

(defn execute-object-ruleset [config ruleset nspace]
  (if-let [next-config
           (apply-first-applicable-object config ruleset nspace)]
    (let [_ (assert (not= next-config config))]
      (recur next-config ruleset nspace))
    [config])) ; return config*, so it has the same return type as the meta-ruleset version

(defmulti execute-ruleset
  (fn [rset _ _]
    (ruleset-type rset)))

(defmethod execute-ruleset :meta [rset config nspace]
  ; TODO logging here
  (execute-meta-ruleset config (:rules rset) nspace))

(defmethod execute-ruleset :object [rset config nspace]
  (execute-object-ruleset config (:rules rset) nspace))



