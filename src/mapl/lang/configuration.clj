;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.configuration
  (:require [mapl.lang.manifest :as mani]
            [mapl.lang.term :as t]
            [clojure.pprint :as pp]
            [clojure.data :refer [diff]])
  (:refer-clojure :exclude [cycle] :rename {atom clj-atom}))

(def counter (clj-atom -1))

(defn fresh-id []
  (t/id (swap! counter inc)))

(def id :id)
(def cycle :cycle)
(def atom :atom)
(def plan :plan)
(def remainder :remainder)
(def state :state)
(def substitution :substitution)
(def originator :originator)

(def manifest :manifest)
(def phase :phase)
(def last-id-created :last-id-created)

(def empty-manifest [])

(defn str-plan [p]
  (if (empty? p)
    (str [])
    (apply str
           (interpose "; "
                      (for [a p]
                        (cond
                          (t/query-action? a)
                          (str "?" (t/str-logexpr (:value a)))

                          (t/subgoal-action? a)
                          (str "!" (t/str-atom (:value a)))

                          (t/atomform-action? a)
                          (t/str-atom (:value a))))))))

(defn pretty-ai [ai]
  (-> ai
      (update-in [atom] t/str-atom)
      (update-in [id] t/str-id)))

(defn pretty-pi [pi]
  (-> pi
      (update-in [plan] str-plan)
      (update-in [id] t/str-id)
      (update-in [remainder] str-plan)
      (update-in [originator] (fn [[rs r]]
                                (str "rs" rs " r" r)))
      (update-in [state]
                 (fn [flags]
                   (apply str
                          (interpose ", " (map t/str-atom flags)))))))


(defn str-ai [ai]
  (-> ai pretty-ai str))

(defn str-pi [pi]
  (-> pi pretty-pi str))


(def blank-config
  {cycle 0
   :phase 0
   :atom-instances {}
   :plan-instances {}
   :subgoals #{}
   :justifications #{}
   last-id-created nil
   manifest empty-manifest})

(defn add-event-to-manifest [config event]
  (update-in config [manifest] #(conj % event)))

(defn add-ai 
  ([config ai]
   {:pre [(not (contains? (:atom-instances config) (id ai)))
          (t/id? (id ai))]}
   (-> config
       (assoc-in [:atom-instances (id ai)] ai)
       (add-event-to-manifest (mani/->addition-event "atom instance" (id ai) (str-ai ai)))))

  ([config i a tags]
   (let [c (cycle config)]
     (add-ai
       config
       {atom a
        id i
        cycle c
        :tags (set tags)})))

  ([config a tags]
   (let [id (fresh-id)]
     (add-ai
       (assoc config last-id-created id)
       id a tags))))

; replace 'c' arg by just getting it from config
(defn add-pi 
  ([config pi]
   ;{:pre [(not (contains? (:plan-instances config) (:id pi)))]}
   (-> config
       (assoc-in [:plan-instances (:id pi)] pi)
       (add-event-to-manifest (mani/->addition-event "plan instance" (id pi) (str-pi pi)))))

  ([config i p fs s origin]
   (add-pi
     config
     {plan p
      remainder p
      id i
      cycle (cycle config)
      state (set fs)
      substitution s
      originator origin}))

  ([config p fs s origin]
   (let [id (fresh-id)]
     (add-pi
       (assoc config last-id-created id)
       id p fs s origin))))

(defn atom-instances [config]
  (vals (:atom-instances config)))

(defn plan-instances [config]
  (vals (:plan-instances config)))

(defn atom-instance [config i]
  (get (:atom-instances config) i))

(defn plan-instance [config i]
  (get (:plan-instances config) i))

(defn del-ai [config i]
  (-> config
      (update-in [:atom-instances] #(dissoc % i))
      (add-event-to-manifest (mani/->deletion-event "atom instance" i (str-ai (atom-instance config i))))))

(defn del-pi [config i]
  (-> config
      (update-in [:plan-instances] #(dissoc % i))
      (add-event-to-manifest (mani/->deletion-event "plan instance" i (str-pi (plan-instance config i))))))


(defn add-instance-update-events [datatype config change-map id]
  (reduce
    (fn [config [changed-field new-value]]
      (add-event-to-manifest config
                             (mani/->update-event datatype
                                                  id
                                                  changed-field
                                                  new-value)))
    config
    (seq change-map)))

(defn update-ai [config i f]
  (let [c 
        (-> config
            (update-in [:atom-instances i] f))

        [_ changes _] (clojure.data/diff 
                        (atom-instance config i)
                        (atom-instance c i))]
    (add-instance-update-events "atom instance" c changes i)))


(defn update-pi [config i f]
  (let [c 
        (update-in config [:plan-instances i] f)

        [_ changes _] (clojure.data/diff
                        (plan-instance config i)
                        (plan-instance c i))]
    (add-instance-update-events "plan instance" c changes i)))


(def tags :tags)
(def goal :goal)
(def percept :percept)
(def top-level :top-level)

(defn goal? [ai]
  (contains? (:tags ai) goal))

(defn percept? [ai]
  (contains? (:tags ai) percept))

(defn top-level? [ai]
  (contains? (:tags ai) top-level))

(defn percepts [config]
  (filter percept? (atom-instances config)))

(defn justifications [config]
  (let [res (-> config :justifications seq)]
    (if (empty? res)
      []
      res)))

(defn subgoals [config]
  (let [res (-> config :subgoals seq)]
    (if (empty? res)
      []
      res)))
; both are these are sets of tuples
; left is plan's id, always

(defn plan-justifications [config i]
  (for [[plan-id j] (justifications config)
        :when (= i plan-id)]
    j))

(defn justified-plans [config i]
  (for [[plan-id j] (justifications config)
        :when (= i j)]
    plan-id))

(defn child-goals [config i]
  (for [[plan-id sg] (subgoals config)
        :when (= i plan-id)]
    sg))

(defn lookup-parent [config i]
  (first
    (for [[plan-id sg] (subgoals config)
          :when (= i sg)]
      plan-id)))

; delete a plan's subgoal relations
(defn del-plan-subgoals [config i]
  (let [match
        (fn [[i' _]] (= i i'))]
    (-> config
        (update-in [:subgoals]
                   #(set (remove match (seq %))))
        (add-event-to-manifest (mani/->deletion-event "subgoal relations" i (str "plan instance " i "'s subgoal relations"))))))

; delete a subgoal's subgoal relations
(defn del-subgoal-relation [config i]
  (let [match
        (fn [[_ i']] (= i i'))]
    (-> config
        (update-in [:subgoals]
                   #(set (remove match (seq %))))
        (add-event-to-manifest (mani/->deletion-event "subgoal relations" i (str "atom instance (goal) " i "'s subgoal relations"))))))

; delete an atom's justification relations
(defn del-just-relations [config i]
  (let [match
        (fn [[_ i']] (= i i'))]
    (-> config
        (update-in [:justifications]
                   #(set (remove match (seq %))))
        (add-event-to-manifest (mani/->deletion-event "justification relations" i (str "atom instance " i "'s justification relations"))))))

; delete a plan's justification relations
(defn del-plan-just-relations [config i]
  (let [match
        (fn [[i' _]] (= i i'))]
    (-> config
        (update-in [:justifications]
                   #(set (remove match (seq %))))
        (add-event-to-manifest (mani/->deletion-event "justification relations" i (str "plan instance " i "'s justification relations"))))))

(defn add-subgoal
  ([config a plan-id]
   (let [i (fresh-id)]
     (add-subgoal config i a plan-id)))

  ([config i a plan-id]
   (let [config (add-ai config i a #{goal})
         sg-id i]
     (-> config
         (update-in [:subgoals]
                    #(set (conj (set %) [plan-id sg-id])))
         (add-event-to-manifest (mani/->addition-event "subgoal relation" [plan-id sg-id](str "atom instance " sg-id " as a subgoal of plan instance " plan-id)))))))

(defn add-justifications [config planid js]
  (reduce
    (fn [config j]
      (-> config
          (update-in [:justifications]
                     #(set (conj (set %) [planid j])))
          (add-event-to-manifest (mani/->addition-event "justification relation" [planid j] (str "atom instance " j " as a justification of plan instance " planid)))))
    config
    js))

(defn add-flag [config planid f]
  (-> config
      (update-in [:plan-instances planid state]
                 #(set (conj (set %) f)))
      (add-event-to-manifest (mani/->update-event "plan instance" planid "state" (str "include the '" (t/str-atom f) "' flag")))))

(defn remove-flag [config planid f]  
  (-> config
      (update-in [:plan-instances planid state]
                 #(disj (set %) f))
      (add-event-to-manifest (mani/->update-event "plan instance" planid "state" (str "remove the '" (t/str-atom f) "' flag")))))


;;; equivalence
(declare atom-eq)

(defn match-args [ais as1 as2]
  (when (= (count as2)
           (count as2))
    (reduce
      (fn [_ [arg1 arg2]]
        (cond 
          (and (t/id? arg1)
               (t/id? arg2))
          (atom-eq ais
                   (atom (get ais arg1))
                   (atom (get ais arg2)))

          (= arg1 arg2)
          true

          :else
          (reduced false)))
      true
      (map vector as1 as2))))


(defn atom-eq [ais a1 a2]
  (and (= (t/pred a1)
          (t/pred a2))
       (match-args ais (t/args a1) (t/args a2))))

(declare atom-useful)

(defn arg-useful [ais arg]
  (if (t/id? arg)
    (atom-useful ais (atom (get ais arg)))
    arg))

(defn atom-useful [ais a]
  (vec
   (cons
     (t/pred a)
     (map #(arg-useful ais %) (t/args a)))))

(defn ai-useful [ais ai]
  ((juxt 
     (fn [ai]
       (->> (tags ai) (remove #(= top-level %)) (set)))
     cycle
     (fn [ai]
       (atom-useful ais (atom ai))))
    ai))
 
                  ; TODO
                      ; when checking subgoals, should be using the substitution from the plan instance
                      ; and with justifications, should be doing 'subgoal magic' from primitive_query.clj

(defn compare-goal-plan-structure [c1 c2]
  (when (= (count (plan-instances c1))
           (count (plan-instances c2)))
    (loop [wset1 (plan-instances c1)
           wset2 (set (plan-instances c2))]
      (if (empty? wset1)
        true
        (let [match
              (fn [px py]
                (and 
                  (= (originator px)
                     (originator py)) ; check that they're from the same object rule
                  (= (cycle px)
                     (cycle py)) ; check from same cycle (unless we want legit bisimilarity?)
                  (= (count (remainder px))
                     (count (remainder py))) ; check that they have the same progress
                  (= (set (state px))
                     (set (state py))) ; check that they have the same state flags set
                  (let [jsx (map #(atom-instance c1 %) (plan-justifications c1 (id px))) ; take the justifications for each
                        jsy (map #(atom-instance c2 %) (plan-justifications c2 (id py)))
                        sgx (map #(atom-instance c1 %) (child-goals c1 (id px))) ; and subgoal belonging to each (if any)
                        sgy (map #(atom-instance c2 %) (child-goals c2 (id py)))]
                    (and
                      (= (set (map #(ai-useful (:atom-instances c1) %) jsx)) (set (map #(ai-useful (:atom-instances c2) %) jsy)))
                      (= (set (map #(ai-useful (:atom-instances c1) %) sgx)) (set (map #(ai-useful (:atom-instances c2) %) sgy))))))) ; check that they are equivalent


              pi1 (first wset1)
              matches (filter #(match pi1 %) wset2)
              _ (assert (not (> (count matches) 1)) (clojure.pprint/pprint matches))
              pi2 (first matches)]
          (if (empty? matches)
            false
            (recur (rest wset1) (disj wset2 pi2))))))))


(defn ais-eq [ais1 ais2]
  (= (set (map #(ai-useful ais1 %) (remove percept? (vals ais1))))
     (set (map #(ai-useful ais2 %) (remove percept? (vals ais2))))))

(defn config-eq [c1 c2]
  (and 
    (ais-eq (:atom-instances c1) (:atom-instances c2))
    (compare-goal-plan-structure c1 c2)))
