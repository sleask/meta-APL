;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.eval
  (:require [mapl.lang.term :refer :all]
            [taoensso.timbre :as log]))


(declare -eval)

(defn bound-lvar? [lv s]
  (and (lvar? lv)
       (contains? s lv)))

(defn unbound-lvar? [lv s]
  (and (lvar? lv)
       (not (contains? s lv))))

; return the unbound lvars if they exist
(defn unbound-vars? [expr s]
  (seq
    (distinct
      (filter #(unbound-lvar? % s) (flatten expr))))) ; might need the superior flatten from context.clj. should be okay as is if only applied to arith

(defn eval-arith-expr [expr s]
;  {:post [(or (number? %) (nil? %))]}
  (let [[op l r] expr]
    (when-let [l' (-eval l s)]
      (when-let [r' (-eval r s)]
        (try ((get arith-ops op) (-eval l s) (-eval r s))
             (catch Exception e (log/debug "arith-eval: tried to divide by zero!")))))))

(defn -eval [x s]
  ;(log/debug "eval'ing: " (str x) " with s=" (str s))
  (cond
    (arith-expr? x)
    (if-let [lvars (unbound-vars? x s)]
      (log/warn (str "arith-eval: trying to evaluate non-ground expr! Unbound vars: " lvars " in " (str-arith x)))
      (eval-arith-expr x s))


    (htlist? x) ; make sure this comes before ordinary lists
    (vec
      (cons (-eval (head x) s)
          (-eval (tail x) s)))

    (sequential? x) ; ordinary lists and atoms
    (map #(-eval % s) x)

    (lvar? x)
    (recur (s x) s)

    (atom? x)
    (-atom (-eval (pred x) s)
           (-eval (args x) s))

    (nil? x)
    nil

    (wildcard? x)
    nil

    :else
    x))

