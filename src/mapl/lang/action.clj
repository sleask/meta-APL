;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.action
  (:require [mapl.lang.logic :refer [solve]]
            [mapl.lang.configuration :refer [add-subgoal add-event-to-manifest]]
            [mapl.lang.primitive-action :refer [primitive-action primitive-action?]]
            [mapl.lang.manifest :refer [->execution-event]]
            [mapl.lang.term :as t]
            [mapl.lang.macro :as macro]
            [mapl.lang.unify :refer :all]
            [mapl.lang.environment :as e]
            [mapl.lang.eval :refer [-eval]]
            [taoensso.timbre :as log]))


; instead of returning either a success or failure wrapped value
; pass a function for what to do in the event of a failure
; and a function for success

; this is just CPS

; just before execution, each of these will be transformed into
; 'actions' as below
; at that time, use namespace etc to classify as either macro-call,
; primitive-call, or external-call

(def subgoal-action t/subgoal-action)
(def atomform-action t/atomform-action)
(def query-action t/query-action)

(defn error-state [msg state]
  [msg state])

;(defn primitive-action [a config s]
; [config s])

(def config :config)
(def substitution :substitution)
(def environment :environment)

(defn execute-plan-action 
  ([act c env s]
   (execute-plan-action
     act
     {config c
      environment env
      substitution s}))

  ([act state]
   (act state identity identity)))


(defn query [expr nspace]
  (fn [state success-k failure-k]
    (let [conf (config state)
          s (substitution state)
          result (solve expr conf nspace s)]
      (if (empty? result)
        (failure-k (error-state (str "query action failed!: " expr) state))
        (success-k (assoc state substitution (first result)))))))

(defn subgoal [a pi]
  (fn [state success-k failure-k]
    (success-k
      (update-in state [config] #(add-subgoal % a pi)))))


(defn primitive [a]
  (fn [state success-k failure-k]
    (if-let [[c s] (primitive-action a (config state) (substitution state))]
      (success-k 
        (-> state
            (assoc config c)
            (assoc substitution s)))
      (failure-k state))))

(defn macro-body [acts nspace]
  (fn [state success-k failure-k]
    (let [successor
          (reduce
            (fn [s-k act]
              (fn [state]
                (act state s-k failure-k))) 
            success-k
            (reverse acts))]
      (successor state))))

(declare convert-AST-macro-act)

(defn macro [a nspace]
  (fn [state success-k failure-k]
    {:pre [(macro/exists? nspace a)]}
    (let [macros (map prep-terms (macro/by-atom nspace a))]
      (if-let [[s' b]
               (first
                 (for [[h b] macros
                       :let [s' (unify a h (substitution state))]
                       :when s']
                   [s' b]))]
        ((macro-body (map #(convert-AST-macro-act % nspace) b) nspace)
         (assoc state substitution s')
         success-k
         failure-k)))))

(defn external [a]
  (fn [state success-k failure-k]
    (let [env (environment state) ; TODO state needs refactoring
          n (:name state)
          s (substitution state)
          _ (assert (some? env))
          _ (assert (some? n))]
      (if (not (e/external-action? env n (-eval a s)))
        (throw (Exception. (str "trying to execution external action, but does not exist: " a)))

        (let [a (-eval a s)
              result (e/external-action env n a)]
          (cond
            (e/success? result)
            (success-k (-> state
                           (assoc environment (:value result))
                           (update-in [config] #(add-event-to-manifest % (->execution-event
                                                                           "external action"
                                                                           (-eval a s)
                                                                           true
                                                                           nil)))))


            (e/failed? result)
            (do
              (log/debug "external action (" (t/str-atom a) ") failed: " (:reason result))
              (failure-k (error-state (:reason result)
                                      (-> state 
                                          (assoc environment (:value result))
                                          (update-in [config] #(add-event-to-manifest % (->execution-event
                                                                                          "external action"
                                                                                          (-eval a s)
                                                                                          nil)))))))

            :else
            (throw (Exception. (str "bad result from env: " result)))))))))

(declare convert-AST-metarule-act)

(defn for-each [[x ys a] nspace]
  (fn [state success-k failure-k]
    (let [successor
          (reduce
            (fn [s-k [act s]]
              (fn [state]
                (act (assoc-in state [substitution] s) s-k failure-k)))
            success-k
            (for [y ys
                  :let [s (unify x y (substitution state))
                        a (-eval a s)
                        act (t/atomform-action a)]]
              (vector (convert-AST-metarule-act act nspace) s)))]
      (successor state))))


(defmulti convert-AST-macro-act
  (fn [act nspace]
    (:type act)))

(defmethod convert-AST-macro-act :query [act nspace]
  (query (:value act) nspace))

(defmethod convert-AST-macro-act :atomform [act nspace]
  ; use nspace to determine whether it's a primitive or another macro
  ; if it's neither, need to throw error
  (cond
    (primitive-action? (:value act))
    (primitive (:value act))

    (macro/exists? nspace (:value act))
    (macro (:value act) nspace)

    :else
    (throw (Exception. (str "not a primitive or macro: " ;(str-atom (:value act)))))))
                            act)))))

(defmethod convert-AST-macro-act :default [act nspace]
  (throw (Exception. (str "not a valid query, macro, or external action: " (str (:value act))))))     

(defmulti convert-AST-plan-act
  (fn [act nspace planid]
    (:type act)))

(defmethod convert-AST-plan-act :query [act nspace _]
  (query (:value act) nspace))

(defmethod convert-AST-plan-act :subgoal [act _ planid]
  (subgoal (:value act) planid))

(defmethod convert-AST-plan-act :atomform [act nspace _]
  (cond
    (primitive-action? (:value act))
    (primitive (:value act))

    (macro/exists? nspace (:value act))
    (macro (:value act) nspace)

    ; since env is in the state map
    ; and we need it to look up whether
    ; external actions exist... 
    ; have to default to handling as external
    ; at this point
    :else
    (external (:value act))))


(defmethod convert-AST-plan-act :default [act nspace _]
  (throw (Exception. (str "not a valid query, macro, subgoal, or external action: " (str act)))))     


(defmulti convert-AST-metarule-act
  (fn [act _]
    (:type act)))

(defmethod convert-AST-metarule-act :atomform [act nspace]
  (cond
    (primitive-action? (:value act))
    (primitive (:value act))

    (macro/exists? nspace (:value act))
    (macro (:value act) nspace)

    (not (fail? (unify (:value act) (t/-atom "for-each" [(t/lvar "X") (t/lvar "Y") (t/lvar "Z")]) {})))
    (for-each (t/args (:value act)) nspace)

    :else
    (throw (Exception. (str "not a valid primitive operation or macro: " (t/str-atom (:value act)))))))    

(defmethod convert-AST-metarule-act :default [act nspace]
  (throw (Exception. (str "not a valid primitive operation or macro: " (str act)))))     



