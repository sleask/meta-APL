;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.clause
  (:require [mapl.lang.unify :as u]
            [mapl.lang.term :refer :all]))

(defn head->key [h]
  (keyword (str (pred h) "|" (count (args h)))))

(defn add-binding [nspace head expr]
  {:pre [(atom? head)]}
  (let [k (head->key head)]
    (update-in nspace [:clauses k] #(vec (conj (vec %) [head expr])))))
; need to preserve order

(defn by-atom [nspace a]
  (get-in nspace [:clauses (head->key a)]))

(defn exists? [nspace a]
  (contains? (:clauses nspace) (head->key a)))

(defn unify-with-all [nspace a s]
  ;(let [clauses (apply concat (vals (:clauses nspace)))]
    (for [clauses (vals (:clauses nspace))
          [head expr] clauses
          ; gen a seq of pairs, [s', seeded-expr] from successful unification
          ; with (seeded) head
          :let [;seed (u/fresh)
                [h e] (u/prep-terms [head expr])
                s' (u/unify a h s)]
          :when s']
      [s' e]))



