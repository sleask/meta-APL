;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.print
  (:require [mapl.lang.term :as term]
            [mapl.lang.configuration :as cfg]
            [mapl.lang.agent :as agent]
            [mapl.lang.mas :as mas]
            [mapl.lang.action :as act]
            [clojure.pprint :as pp]
            [mapl.lang.manifest :as mani]))

(defn str-env [env]
  (with-out-str (pp/pprint env)))

; moved while refactoring
(def str-atom term/str-atom)
(def str-plan cfg/str-plan)
(def pretty-ai cfg/pretty-ai)
(def pretty-pi cfg/pretty-pi)

(defn print-ais [ag]
  (println (str "Agent " (agent/name ag) "'s atom instances:"))
  (pp/print-table
    [cfg/id cfg/atom cfg/cycle cfg/tags]
    (->>
      (-> ag agent/config cfg/atom-instances)
      (map pretty-ai)
      (sort-by :cycle))))

(defn print-pis [ag]
  (println (str "Agent " (agent/name ag) "'s plan instances:"))
  (pp/print-table
    [cfg/id cfg/remainder cfg/originator cfg/cycle cfg/state]
    (->>
      (-> ag agent/config cfg/plan-instances)
      (map pretty-pi)
      (sort-by :cycle)))) ; TODO show justifications and subgoals

(defn print-manifest [manifest]
  (doseq [event manifest]
    (println (mani/to-string event))))

(defn print-manifests [agents]
  (doseq [ag agents]
    (do
      (println (str "Changes in " (agent/name ag) ":"))
      (print-manifest (-> ag agent/config cfg/manifest)))))
