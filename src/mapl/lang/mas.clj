;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.

(ns mapl.lang.mas
  (:require [mapl.lang.agent :as agent]
            [clojure.edn :as edn]
            [mapl.lang.environment :as e]
            [mapl.lang.rule :as rule]
            [mapl.lang.configuration :as cfg]
            [clojure.pprint :as pp])
  (:gen-class))

(def agents :agents)
(def environment :environment)

(defn make-mas [agents env]
  {:agents agents
   :environment env})

(defn deterministic-cycle [mas]
  (loop [env (:environment mas)
         to-cycle (:agents mas)
         done []]
    (if-let [a (first to-cycle)]
      (let [[a e] (first (agent/cycle a env))]
        (recur e (rest to-cycle) (vec (conj done a))))
      (-> mas
          (assoc :environment env)
          (assoc :agents done)))))

(defn deterministic-trace [mas]
  (iterate deterministic-cycle mas))

;(defn basic-sense [ag env]
;    (let [as (e/sense env (agent/name ag))]
;      (agent/add-percepts ag as)))

(defn basic-sense [ag env]
  (let [as (e/sense env (agent/name ag))]
    (update-in ag [agent/config] #(e/belief-update env % as))))

(defn load-agent [n path]
  (-> path
      (agent/parse-agent-file)
      (assoc agent/name n)))

(defn load-mas [s]
  (let [m 
        (edn/read-string s)]
    (do
      (let [nsp (load-file (:environment-source m))
            agents (map #(apply load-agent %) (seq (:agents m)))
            agents  (map #(assoc % agent/sensefn basic-sense) agents)
            env (apply
                  (eval (symbol (str (->> nsp meta :ns) "/" (:environment-name m))))
                  agents)]
        (make-mas agents env)))))
