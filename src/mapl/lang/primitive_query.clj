;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.primitive-query
  (:require [mapl.lang.term :refer :all]
            [mapl.lang.unify :refer :all]
            [mapl.lang.eval :refer [-eval]]
            [mapl.lang.configuration :as cfg]
            [taoensso.timbre :as log]))

(def tracked-justs :tracked-justs)

(defn clear [results]
  (distinct
    (remove fail? results)))

; may need a custom 'distinct' to avoid effects of 
; the accumulated justifications on substitution equality
; hacky

; *SUBGOAL-MAGIC*

(def no-results [])

(defn into-results [xs]
  (if (sequential? xs)
    (vec (remove fail? xs))
    (recur (vector xs))))

(defn join-results [r1 r2]
  (cond
    (empty? r1)
    r2

    (empty? r2)
    r1

    :else
    (distinct
      (lazy-seq
        (cons (first r1)
              (join-results r2 (rest r1)))))))

(defn state
  ([config s justifying?]
   [config s justifying?])

  ([config s]
   (state config s false)))

(def config first)
(def substitution second)


(defn subgoal-magic 
  "when matching against a subgoal, merge the parent substitution with s (if it exists)"
  [config i s]
    (if (not (cfg/goal? (get-in config [:atom-instances i])))
      s
      (if-let [parent (cfg/lookup-parent config i)]
        (let [parent-s (get-in config [:plan-instances parent cfg/substitution])]
        (do
        ;  (log/warn "merging" (str s) "\nwith\n" (str parent-s))
          (let [_ (assert (and (map? parent-s) (map? s)))]
        (merge parent-s s))))
        (do
          ;(log/warn "goal, but couldn't get parent")
          s))))


(defn i-a-c [ai]
  [(cfg/id ai)
   (cfg/atom ai)
   (cfg/cycle ai)])


; need to (re)add in fast-atom later
(defn atom-3 [i a c]
  (fn [[config s justifying?]]
    (clear
      (for [[i' a' c']
            (map i-a-c (cfg/atom-instances config))
            :let [s (subgoal-magic config i' s)]]
        (if-let [s'
                 (unify c c'
                        (unify a a'
                               (unify i i' s)))]
          (if justifying?
            (update-in s' [tracked-justs] #(conj (set %) i'))
            s')
          fail)))))

(defn atom-2 [i a]
  (atom-3 i a wildcard))

(defn atom-1 [a]
  (atom-2 wildcard a))

(defn cycle-2 [i n]
  (fn [[config s _]]
    (clear
      (for [inst (lazy-cat (cfg/atom-instances config)
                             (cfg/plan-instances config))]
                 (unify n (cfg/cycle inst)
                   (unify i (cfg/id inst) s))))))

(defn cycle-1 [c]
  (fn [[config s _]]
    (clear
      (into-results
        (let [c' (cfg/cycle config)]
          (unify c c' s))))))

(defn generic-plan-relation [access]
  (fn [i x]
    (fn [[config s _]]
      (clear
          (for [pi (cfg/plan-instances config)
                :let [i' (cfg/id pi)
                      x' (access pi)]]
            (unify x x'
              (unify i i' s)))))))

(def plan-2
  (generic-plan-relation cfg/plan))

(def remainder-2
  (generic-plan-relation cfg/remainder))

(def state-2
  (generic-plan-relation cfg/state))

(def substitution-2
  (generic-plan-relation cfg/substitution))

(defn justification-2 [i j]
  (fn [[config s _]]
    (clear
      (for [[i' j'] (cfg/justifications config)]
        (unify j j'
               (unify i i' s))))))

(defn subgoal-2 [i sg]
  (fn [[config s _]]
    (clear
      (for [[i' sg'] (cfg/subgoals config)]
        (unify sg sg'
               (unify i i' s))))))

;;;;


(defn member-2 [x xs]
  (fn [[_ s _]]
    (if-let [xs' (-eval xs s)]
      (clear
        (map #(unify x % s) xs'))
      (into-results
        (unify xs [x] s)))))

(defn merge-3 [x y z]
  (fn [[_ s _]]
    (let [x' (-eval x s)
          y' (-eval y s)]
      (if (and x' y')
        (let [res (unify z (clojure.set/union (set x') (set y')) s)]
            (into-results res))
        fail))))

(defn max-2 [xs n]
  (fn [[_ s _]]
    (let [xs' (-eval xs s)
          result (apply max (set (map int xs)))]
      (into-results
        (unify n result s)))))

(defn true-0 []
  (fn [[_ s _]]
    (into-results s))) 

(defn false-0 []
  (fn [[_ _ _]]
    no-results))



(def primitive-queries
  {"atom|3" atom-3
   "atom|2" atom-2
   "atom|1" atom-1
   "cycle|2" cycle-2
   "cycle|1" cycle-1
   "plan|2" plan-2
   "remainder|2" remainder-2
   "state|2" state-2
   "substitution|2" substitution-2
   "justification|2" justification-2
   "subgoal|2" subgoal-2
   "member|2" member-2
   "merge|3" merge-3
   "true|0" true-0
   "false|0" false-0})

(defn atom->key [a]
  (str (pred a) "|" (count (args a))))

(defn primitive-query? [a]
  (contains? primitive-queries (atom->key a)))

(defn primitive-query [a config s justifying?]
  (let [rel (get primitive-queries (atom->key a))
        _ (assert (not (nil? rel)))]
      ((apply rel (args a))
        (state config s justifying?))))


