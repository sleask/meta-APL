;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns mapl.lang.logic
  (:require [mapl.lang.eval :refer :all]
            [mapl.lang.unify :refer :all]
            [mapl.lang.term :refer :all]
            [mapl.lang.clause :as clause]
            [clojure.walk :refer [postwalk]]
            [taoensso.timbre :as log]
            [mapl.lang.primitive-query :as pq]))

(def justs pq/tracked-justs)

(def no-results pq/no-results)

(def into-results pq/into-results)

(def join-results pq/join-results)

(defn negation-as-failure [results old-substitution]
  (if (empty? results)
    (into-results old-substitution)
    no-results))

; relation : args -> state -> [s]

(def state pq/state)

(def config pq/config)
(def substitution pq/substitution)

(defn garbage-collect [s-before args s]
  ; where args are those from calling a clause or primitive query
  ; remove anything that was added by calling the clause or primitive query
  ; which connected to the arguments
  (let [accessible-set
        (reduce 
          (fn [as a]
            (loop [v a
                   working-set as]
              (if (and 
                    (lvar? v)
                    (contains? s v))
                (recur (s v) (conj working-set v))
                (if (lvar? v)
                  (conj working-set v)
                  working-set))))
          #{}
          args)]

    (reduce
      (fn [s k]
        (if (and
                (not (contains? s-before k))
                (not (contains? accessible-set k)))
          (dissoc s k)
          s))
      s
      (keys s))))

(defn and-rel [& rels]
  (fn [[c s]]
    (reduce
      (fn [ss rel]
        (mapcat (comp rel #(state config %)) ss))
      [s]
      rels)))

(declare solve)

(defn solve-relex [[op l r] config s]
  (if ((get rel-ops op)
       (-eval l s)
       (-eval r s))
    [s]
    no-results))

(defn solve-clause [a config nspace s]
  {:pre [(clause/exists? nspace a)]}
  (let [clauses (map prep-terms (clause/by-atom nspace a))]
    (if-let [[s' h e]
             (first
               (for [[h e] clauses
                     :let [s' (unify a h s)]
                     :when s']
                 [s' h e]))]
     ; (distinct
     ;   (map #(garbage-collect s' (rest h) %)
     ;      (solve e config nspace s')))
      (solve e config nspace s')
      no-results)))

; TODO garbage collection for primitive queries
; garbage collection on clause call may hit performance
; only worth doing after the top-level call

(declare meta-query? meta-query)

(defn solve-atom [a config nspace s justifying?]
  (cond
    ; is it in the namespace?
    (clause/exists? nspace a)
    (let [res (solve-clause a config nspace s)]
      (if (empty? res)
        (solve-atom ["atom" a] config nspace s justifying?)
        res))

    ; is it a primitive?
    (pq/primitive-query? a)
    (pq/primitive-query a config s justifying?)

    ; meta-query?
    (meta-query? a)
    (meta-query a config s nspace)

    ; else interpret as atom(_,a)
    :else
    (solve-atom ["atom" a] config nspace s justifying?)))


(defmulti solve 
  (fn [expr _ _ s]
    (first expr)))

(defmethod solve :and [expr config nspace s]
  (let [[_ l r] expr]
    (->> s
         (solve l config nspace)
         (mapcat #(solve r config nspace %)))))

(defmethod solve :or [expr config nspace s]
  (let [[_ l r] expr]
    (join-results ; swapping this with interleave breaks function, why?
                  (solve l config nspace s)
                  (solve r config nspace s))))

(defmethod solve :not [expr config nspace s]
  (let [[_ ex] expr]
    (negation-as-failure
      (solve ex config nspace s)
      s)))

(defmethod solve :default [expr config nspace s]
  ; either relex or atom-form
  (cond
    (rel-expr? expr)
    (solve-relex expr config s)

    (atom? expr)
    (solve-atom expr config nspace s false)))


; meta-queries
; TODO needs refactoring and tidying (very hacky)

(defn state [c s nspace]
  [c s nspace])

; TODO this simpler version of -eval should probably be used in other places too
; as it sidesteps the issue of recursively evaluating terms
; whilst still solving the problem of resolving arguments to query implementation

(defn ground [expr s]
  (if (lvar? expr)
    (ground (s expr) s)
    expr))

(defn eval-query-3 [expr s-pre s-post]
  ; assume s-post is unbound for sanity's sake
  ; return primitive-query style states
  (fn [[c s n]]
    (let [expr (ground expr s)
          s-pre (ground s-pre s)
          results (solve expr c n s-pre)]
      (map #(unify s-post % s) results))))


(defn findall-3 [x expr xs]
  (fn [[c s n]]
    (let [expr (ground expr s)
          results (solve expr c n s)
          xs' (for [smap results
                    :when (not (fail? smap))]
                (-eval x smap))]
      (into-results
        (unify xs xs' s)))))


(def meta-queries
  {"eval-query|3" eval-query-3
   "findall|3" findall-3})

(def atom->key pq/atom->key)

(defn meta-query? [a]
  (contains? meta-queries (atom->key a)))

(defn meta-query [a config s nspace]
  (let [rel (get meta-queries (atom->key a))
        _ (assert (not (nil? rel)))]
      ((apply rel (args a))
        (state config s nspace))))

