;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns ^{:skip-aot true} mapl.examples.mars-robots.marsenv
  (:require [mapl.lang.environment :refer :all]
            [mapl.lang.parser :as p]
            [mapl.lang.term :refer :all]
            [taoensso.timbre :as log]
            [mapl.lang.configuration :as cfg]))

(def r1 "r1")
(def r2 "r2")

(defn init-state-r1 [item]
  {:holding item})

(defn init-state-r2 []
  nil)
  
(defn initial-grid [w l]
  {:length l
   :width w
   :coords {}})

(def garbage :garbage)
(def nothing nil)


(defn remove-first [f [head & tail]]
   (if (not head) []
   (if (f head)
       (if (seq tail) tail [])
       (cons head (lazy-seq (remove-first f tail))))))

(defn r1-held-item [env]
  (get-in env [:r1-state :holding]))

(defn agent-coord [grid ident]
  (ffirst
    (filter (comp #(% ident) set second) (:coords grid))))

(defn add-object [grid identifier [x y]]
  (update-in grid [:coords [x y]] #(conj % identifier)))

(defn del-object [grid identifier [x y]]
  (update-in grid [:coords [x y]] #(remove-first #{identifier} %)))

(defn add-garbage [grid [x y]]
  (add-object grid garbage [x y]))

(defn add-agent [grid ident [x y]]
  (add-object grid ident [x y]))

(defn del-agent [grid ident]
  (let [curr-coord (agent-coord grid ident)]
    (del-object grid ident curr-coord)))

(defn item-here? [grid ident coord]
  (contains? (set (get-in grid [:coords coord]))
             ident))

(defn garbage-here? [grid coord]
  (item-here? grid garbage coord))

(defn no-garbage? [env]
  (and
    (not
      (contains?
        (set (flatten (vals (:coords (:grid env)))))
        garbage))
    (not
      (= (r1-held-item env) garbage))))

(defn r1-loc [grid]
  (agent-coord grid r1))

(defn r2-loc [grid]
  (agent-coord grid r2))



(defn -next [grid n]
     (let [w (:width grid)
        l (:length grid)
        [x1 y1] (agent-coord grid n)

        inc-y (if (>= (inc y1) l)
                0
                (inc y1))
        [x2 y2]
        (if (>= (inc x1) w)
                [0 inc-y]
                [(inc x1) y1])]
        
  (-> grid
      (del-agent n)
      (add-agent n [x2 y2]))))


(defn pick [env n x]
  (when (= nothing (r1-held-item env))
    (let [grid (:grid env)
          curr-coord (agent-coord grid r1)
          x (if (= x :garb)
              garbage
              x)]
        (when (item-here? grid x curr-coord)
          (-> env
              (update-in [:grid] #(del-object % x curr-coord))
              (assoc-in [:r1-state :holding] x))))))


(defn -drop [env n x]
  ; handle the Jason version of the call specially
  (let [x (if (= x :garb)
            garbage
            x)]
  (when (= x (r1-held-item env))
    (let [curr-coord (agent-coord (:grid env) r1)]
      (-> env
          (update-in [:grid] #(add-object % x curr-coord))
          (assoc-in [:r1-state :holding] nothing))))))

(defn move-agent [grid ident new-coord]
  ; check bounds
    (-> grid
        (del-agent ident)
        (add-agent ident new-coord)))


(defn move-towards [grid n [x y]]
  (let [[x1 y1] (agent-coord grid n)

        y2 (cond 
             (= y1 y)
             y1

             (> y1 y)
             (dec y1)

             (< y1 y)
             (inc y1))

        new-coord (if (= x1 x)
                    [x1 y2]
                    (cond
                      (> x1 x)
                      [(dec x1) y1]

                      (< x1 x)
                      [(inc x1) y1]))]

    (move-agent grid n new-coord)))

(defn burn [env _ x]
  (when (and
          (garbage-here? (:grid env) (r2-loc (:grid env)))
          (= x (-atom "garb")))
    (assoc env :grid
           (del-object (:grid env) garbage (r2-loc (:grid env))))))


(def actions-r1
  #{["next" 1] 
   ["pick" 1] 
   ["drop" 1]
   ["move_towards" 2]})

(def actions-r2
  #{["burn" 1]})

(defn exists? [a n]
  (if (= n "r1")
    (contains? actions-r1 [(pred a) (count (args a))])
    (contains? actions-r2 [(pred a) (count (args a))])))

(defn action-r2 [env a]
  (condp = [(pred a) (count (args a))]
    ["burn" 1]
    (if-let [env (burn env r2 (first (args a)))]
      (success env)
      (failure env "burn failed"))

    (failure env (str (str-atom a) " is not a valid action."))))


(defn action-r1 [env a]
  (condp = [(pred a) (count (args a))]
    ["next" 1]
    (if (= (first (args a)) (-atom "slot"))
      (if-let [grid (-next (:grid env) r1)]
        (success (assoc env :grid grid))
        (failure env "couldn't move to next cell!"))
      (failure env (str [(pred a) (args a)] " is not a valid action.")))

    ["move_towards" 2]
    (let [[x y] (args a)]
      (if-let [grid (move-towards (:grid env) r1 (args a))]
        (success (assoc env :grid grid))
        (failure env "could not move to ["x","y"].")))

    ["pick" 1]
    (if-let [env (pick env r1 (keyword
                                (pred
                                  (first (args a)))))]
      (success env)
      (failure env "pick failed"))

    ["drop" 1]
    (if-let [env (-drop env r1 (keyword
                                 (pred
                                   (first (args a)))))]
      (success env)
      (failure env "drop failed"))


    (failure env (str (str-atom a) " is not a valid action."))))  

(defn r1-sense [env]
  (let [holding
        (if-let [held-item (r1-held-item env)]
          (p/parse-atom (str "belief(holding(" (name held-item) "))"))
          (p/parse-atom "belief(holding(nothing))"))

        r1-pos
        (let [[x y] (r1-loc (:grid env))]
          (p/parse-atom (str "belief(pos(r1," x "," y"))")))

        r2-pos 
        (let [[x y] (r2-loc (:grid env))]
          (p/parse-atom (str "belief(pos(r2," x "," y"))")))

        garbage?
        (when (garbage-here? (:grid env) (r1-loc (:grid env)))
          (p/parse-atom (str "belief(garbage(r1))")))

        finished?
        (when (no-garbage? env)
          (p/parse-atom (str "belief(finished)")))]

    (remove nil?
            (vector
              holding
              r1-pos
              r2-pos
              garbage?
              finished?))))

(defn r2-sense [env]
  (let [garbage?
        (when (garbage-here? (:grid env) (r2-loc (:grid env)))
          (p/parse-atom "belief(garbage(r2))"))]
    (remove nil?
            (vector
              garbage?))))

(defn -sense [env n]
  (if (= n r1)
    (r1-sense env)
    (r2-sense env)))

(defn add-belief-and-event-addition [config p]
  (-> config
    (cfg/add-ai p #{cfg/percept})
    (cfg/add-ai (-atom (str "+" (pred p)) (args p)) #{})))

(defn delete-belief-and-event-addition [config ai]
  (-> config
      (cfg/del-ai (cfg/id ai))
      (cfg/add-ai (-atom (str "-" (pred (cfg/atom ai))) (args (cfg/atom ai))) #{})))

; optional; if not specified, then assume just add percepts to atom instances each cycle
; but user should be allowed to specify update function if they want
(defn belief-update-function [config new-percepts]
  (let [old-percepts (cfg/percepts config)
        to-delete
        (remove #(contains? (set new-percepts) (cfg/atom %)) old-percepts)

        to-add 
        (clojure.set/difference
                 (set new-percepts) (set (map cfg/atom old-percepts)))

        add-beliefs-and-events
        (fn [c ps]
          (reduce
           add-belief-and-event-addition
           c ps))

        delete-beliefs-and-events
        (fn [c ais]
          (reduce
            delete-belief-and-event-addition
            c ais))]
    (->
      config
      (add-beliefs-and-events to-add)
      (delete-beliefs-and-events to-delete))))


;;;


(defrecord MarsEnv [grid r1-state r2-state]
  Environment
  (sense [this n] (-sense this n))
  (external-action? [this n a] (exists? a n))
  (external-action [this n a]
    (do
      (log/debug "agent " n " executing external-action: " (str-atom a))
    (if (= n r1)
      (action-r1 this a)
      (action-r2 this a))))
  (belief-update [this config new-percepts]
    (belief-update-function config new-percepts))
  (update [this]
    this)
  (observables this))


(def init-grid
  (-> (initial-grid 8 8)
      (add-agent r1 [0 0])
      (add-agent r2 [3 3])
      (add-garbage [3 0])
      (add-garbage [1 2])
      (add-garbage [0 6])
      (add-garbage [7 7])))

(defn init-env [& agents] ; in case we can vary the names and quantity of agents
  (->MarsEnv init-grid (init-state-r1 nothing) (init-state-r2)))

