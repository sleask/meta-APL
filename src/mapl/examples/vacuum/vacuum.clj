;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(ns ^{:skip-aot true} mapl.examples.vacuum.vacuum
  (:require [mapl.lang.environment :refer :all]
            [mapl.lang.parser :as p]
            [mapl.lang.term :refer :all]
            [taoensso.timbre :as log]
            [mapl.lang.agent :refer [default-belief-update]]))

(def r1 "r1")

(def dirty :dirty)
(def clean :clean)

(def initial-grid {0 dirty
                   1 clean
                   2 dirty
                   3 clean})

(defn get-pos-state [env pos]
  (get (:grid env) pos))

(defn set-pos-state [env pos state]
  (assoc-in env [:grid pos] state))

(defn dirty? [env pos]
  (= (get-pos-state env pos) dirty))

(defn clean? [env pos]
  (not (dirty? env pos)))

(defn suck [env]
  (let [ag-pos (:r1-loc env)]
    (set-pos-state env ag-pos clean)))

(defn set-ag-pos [env pos]
  (assoc env :r1-loc pos))

(defn left [env]
  (let [ag-pos (:r1-loc env)]
    (condp = ag-pos
      1 (set-ag-pos env 0)
      3 (set-ag-pos env 2)
      nil)))

(defn right [env]
  (let [ag-pos (:r1-loc env)]
    (condp = ag-pos
      0 (set-ag-pos env 1)
      2 (set-ag-pos env 3)
      nil)))

(defn up [env]
 (let [ag-pos (:r1-loc env)]
    (condp = ag-pos
      2 (set-ag-pos env 0)
      3 (set-ag-pos env 1)
      nil)))

(defn down [env]
  (let [ag-pos (:r1-loc env)]
    (condp = ag-pos
      0 (set-ag-pos env 2)
      1 (set-ag-pos env 3)
      nil)))

(defn clean-or-dirty [env]
  (cond
    (clean? env (:r1-loc env))
    (p/parse-atom "belief(clean)")
    
    (dirty? env (:r1-loc env))
    (p/parse-atom "belief(dirty)")))

(defn pos-percept [env]
  (p/parse-atom (str "belief(pos(" (:r1-loc env) "))")))

(defn sense- [env]
  (vector
    (clean-or-dirty env)
    (pos-percept env)))

(defn exists? [a]
  (contains?
    #{(p/parse-atom "suck")
      (p/parse-atom "left")
      (p/parse-atom "right")
      (p/parse-atom "down")
      (p/parse-atom "up")}
    a))
; may be simpler to give map from atom to function

(defn action [env a]
  (let [env' (condp = a
               (p/parse-atom "suck")
               (suck env)

               (p/parse-atom "left")
               (left env)

               (p/parse-atom "right")
               (right env)

               (p/parse-atom "down")
               (down env)

               (p/parse-atom "up")
               (up env)

               (failure env (str (str-atom a) " is not a valid action!")))]
    (if (nil? env')
      (failure env (str (str-atom a) " failed!"))
      (success env'))))

(defrecord VacuumEnv [grid r1-loc]
  Environment
  (sense [this n]
    (sense- this))

  (external-action? [this n a]
    (exists? a))

  (external-action [this n a]
    (action this a))

  (belief-update [this config new-percepts]
    (default-belief-update config new-percepts))

  (update [this]
    this)

  (observables [this]
    this))



(defn init-env [& agents]
  (->VacuumEnv initial-grid 0))

