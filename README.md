# Meta-APL Interpreter

## Description

This is an interpreter for **meta-APL**, written in Clojure. 

Meta-APL is an agent programming language that supports procedural reflection.
Aspects of an agent's execution cycle are reflected in its state, which may then be queried or updated by the program itself.
The reflection facilities allow the user to encode an agent's deliberation strategy in the same language as the agent program.

Published research on meta-APL includes [this](http://www.cs.nott.ac.uk/~psznza/papers/Doan++:11a.pdf), [this](http://aamas2014.lip6.fr/proceedings/aamas/p149.pdf), and [this](http://eprints.nottingham.ac.uk/31017/).
A comprehensive description of the language can be found in [Thu Trang Doan's thesis](http://eprints.nottingham.ac.uk/29286/).

This codebase is maintained by [me](http://www.cs.nott.ac.uk/~psxsl8) and I can be contacted at svl [at] cs . nott . ac . uk.  
Please feel free to send me any questions or suggestions you have regarding the project.
Raising issues on Gitlab is also welcome.

## Download

Precompiled versions of the interpreter (Java executables in .jar format) can be downloaded from [here](https://gitlab.com/sleask/meta-APL/builds/artifacts/master/download?job=build)

## Running

You will need to have a JRE installed in order to run the interpreter.  
Execute the .jar as you would any Java program:

    java -jar mapl-1.0-standalone.jar [options] path

#### options
Command-line options are as follows:
* **-p** or **--print** (env | ais | pis | delta)*. Pass *env* to print the environment each cycle, *ais* to print the mental state of agents each cycle, *pis* to print the plan state of agents each cycle, *delta* to print a change log for each agent's configuration (per cycle). The default is no printing, except the environment of the final cycle to be executed. Specify multiple print options as a comma separated list eg. env,ais
* **-c** or **--cycle** N. Pass an integer specifying the cycle number to reach before halting. The default is to continue indefinitely.
* **-v** or **--verbosity** LEVEL. Specify the level of verbosity for logging, either *trace*, *debug*, *error*, *warn*, or *info* (see [here](https://github.com/ptaoussanis/timbre) for more details.

## Documentation

A user-guide for the interpreter can be found [here](https://gitlab.com/sleask/meta-APL/uploads/b460d5ec34d6b42dad3508927882d3ef/manual.pdf).
The document gives a description of the language itself, the syntax to be used when writing programs to use with the interpreter, and some details on the procedure for developing enviroments to use with the interpreter.

## Building

[Leiningen](http://leiningen.org/) is required in order to build the source into a Java executable (.jar) containing all the dependencies.

A makefile is provided for building a jar (the 'all' target). The produced jar will be located in the /target/ directory.
Using Leiningen it is possible to build and run the program in a single step, taking the command line options given below, by calling 'lein run *args*'.

It's also possible to install the compiled program to a local Maven repository (for access from Java, for example) with Leiningen (use 'lein install').

Please note that an Internet connection is required for Leiningen to download and cache the project's dependencies, which are listed in the [project manifest](project.clj).

The built jars should appear in the /target/ directory, and the one named 'standalone' is the one that should be used (as it is fully self-contained).

## License

This program is distributed under the Eclipse Public License, a copy of which is provided in the [LICENSE](LICENSE) file.
In case that file is not present for some reason, a copy can be obtained [here](https://www.eclipse.org/legal/epl-v10.html).
