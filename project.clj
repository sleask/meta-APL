;   Copyright (C) 2016 Sam Leask
;   Distributed under the Eclipse Public License.


(defproject mapl "1.1"
  :description "Meta-APL interpreter"
  :url "http://gitlab.com/sleask/meta-APL"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [com.taoensso/timbre "4.2.1"]
                 [funcool/cats "1.2.1"]
                 [org.clojure/core.match "0.3.0-alpha4"]
                 [instaparse "1.4.2"]
                 [org.clojure/math.combinatorics "0.1.1"]
                 [org.clojure/test.check "0.9.0"]
                 [org.clojure/tools.cli "0.3.5"]
                 [environ "1.0.3"]]
  :profiles {:dev {:dependencies [[speclj "3.3.2"]]}
             :uberjar {:aot :all}}
  :plugins [[speclj "3.3.2"]
            [lein-environ "1.0.3"]]
  :test-paths ["spec"]
  :main mapl.main)


