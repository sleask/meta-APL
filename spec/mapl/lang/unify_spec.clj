(ns mapl.lang.unify-spec
  (:require [speclj.core :refer :all]
            [mapl.lang.unify :refer :all]
            [mapl.lang.term :refer :all]
            [mapl.lang.eval :refer [-eval]]))

(describe "generating lvars"
          (it "should not produce the same lvar more than once"
              (should-not= (fresh) (fresh))))

(describe "unification"
          (it "should result in failure if the substitution if failure"
              (should= fail (unify nil nil fail)))

          (it "should associate unbound lvar to value"
              (should= 22 (-eval (lvar "X")
                                 (unify (lvar "X") 22 {})))
              (should-be-nil
                (-eval (lvar "X")
                       (unify (lvar "X") (lvar "Y") {})))

              (should-be-nil
                (-eval (lvar "Y")
                       (unify (lvar "X") (lvar "Y") {})))

              (should= 22 (-eval (lvar "Y")
                                 (unify (lvar "X") (lvar "Y") {(lvar "Y") 22}))))

          (it "should unify wildcard with anything"
              (should-not= fail (unify "test" wildcard {})))

          (it "should unify list and htlist properly"
              (let [s (unify [1 2 3] (htlist (lvar "H") (lvar "T")) {})]
                (should= 1 (-eval (lvar "H") s))
                (should== [2 3] (-eval (lvar "T") s))))

          (it "should unify empty lists"
              (should-not= fail (unify [] [] {})))

          (it "should unify predicate and args for atoms"
              (should-not= fail (unify (-atom (lvar "P") [(lvar "Arg")])
                                       (-atom "test" [1])
                                       {(lvar "P") "test"
                                        (lvar "Arg") 1})))

          (it "should unify with the referenced value when unifying with an lvar (this was previously broken for atoms"
              (should-not= fail (unify (-atom "test" [1 2 3])
                                       (lvar "A")
                                       {(lvar "A") (-atom "test" [1 2 3])})))

          (it "should unify args that are equal"
              (should-not= fail (unify 22 22 {}))
              (should= fail (unify 22 33 {})))

          (it "should fail to unify anything else"
              (should= fail (unify "string" (-atom "string" []) {}))))

(run-specs)
