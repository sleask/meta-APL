(ns mapl.lang.configuration-spec
  (:require [speclj.core :refer :all]
            [mapl.lang.configuration :as cfg]
            [mapl.lang.term :refer [-atom id]]))

(describe "adding an atom instance and deleting it results in equivalent configuration"
          (let [c1 cfg/blank-config
                c2 (cfg/add-ai c1 (-atom "test") #{})
                c3 (cfg/del-ai c2 (cfg/last-id-created c2))]
            (it "should be equivalent to a blank config"
                (should (cfg/config-eq c1 c3)))
            
            (it "should not be equivalent to a blank config"
                (should-not (cfg/config-eq c1 c2)))))

