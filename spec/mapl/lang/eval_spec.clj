(ns mapl.lang.eval-spec
  (:require [speclj.core :refer :all]
            [mapl.lang.eval :refer :all]
            [mapl.lang.term :refer :all]))

(describe "lvars and unbound expressions"

          (let [ex1 [:+ 2 3]
                ex2 [:+ (lvar "X") 3]
                s2 {(lvar "X") 2}
                ex3 [:* (lvar "X") [:+ (lvar "Y") (lvar "Y")]]]

            (it "is/not bound"
                (should (bound-lvar? (lvar "X") s2))
                (should (unbound-lvar? (lvar "Y" s2))))

            (it "expr contains unbound vars"
                (should-contain (lvar "Y") (unbound-vars? ex3 s2)))

            (it "should return correct unbound vars"
                (should== [(lvar "Y")] (unbound-vars? ex3 s2))
                (should-be-nil (unbound-vars? ex2 s2)))))

(describe "evaluation of arithmetic expressions"
          (it "lookup lvars"
              (should= 5 (eval-arith-expr [:+ (lvar "X") (lvar "Y")] {(lvar "X") 2
                                                                      (lvar "Y") 3})))
          (it "don't divide by zero"
              (should-be-nil (eval-arith-expr [:/ 2 0] {}))))

(describe "general evaluation"
          (it "should return nil for lvar that doesn't exist"
              (should-be-nil (-eval (lvar "X") {}))
              (should= 22 (-eval (lvar "X") {(lvar "X") 22})))

          (it "should follow chain of lvars"
              (should= :success
                       (-eval (lvar "X") {(lvar "X") (lvar "Y")
                                          (lvar "Y") (lvar "Z")
                                          (lvar "Z") :success})))

          (it "should evaluate arith to a number, or nil if there are unbound vars"
              (should= 30 (-eval [:* 5 [:+ (lvar "X") (lvar "Y")]] {(lvar "X") 4
                                                                    (lvar "Y") 2}))

              (should-be-nil (-eval [:+ (lvar "X") (lvar "Y")] {})))

          (it "should evaluate the head and tail of an htlist before converting to a list"
              (should== [1 2 3] (-eval (htlist 1 (lvar "T")) {(lvar "T") [2 3]})))

          (it "should evaluate nil to nil"
              (should-be-nil (-eval nil {})))

          (it "should evaluate the wildcard to nil"
              (should-be-nil (-eval wildcard {})))

          (it "should fully evaluate lvars embedded in referenced data structures"
              (let [s 
                    {(lvar "T") [2 (lvar "X")]
                     (lvar "X") 3}]

                (should-be-nil (unbound-vars?
                                 (-eval (htlist 1 (lvar "T")) s)
                                 s))
                (should== [1 2 3]
                          (-eval (htlist 1 (lvar "T")) s))))

          (it "should evaluate to the same value if evaluated again"
              (should= (-eval (htlist 1 [2 3]) {}) (-eval (-eval (htlist 1 [2 3]) {}) {}))))

; TODO redo these with test.check

(run-specs)
