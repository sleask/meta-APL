(ns mapl.lang.term-spec
  (:require [mapl.lang.term :refer :all]
            [speclj.core :refer :all]))

(describe "constructing atoms"
  
  (it "has a predicate"
    (should= "test" (pred (-atom "test" ["and" "args"]))))

  (it "has the correct number of arguments"
    (should= 2 (count (args (-atom "test" ["and" "args"])))))

  (it "satisfies 'atom?'"
    (should (atom? (-atom "test" [["and"] ["args"]])))))

(describe "constructing lists"
  (let [l (htlist 1 [2 3 4 5])]

  (it "should maintain head and tail (and ordering) when converting to list"
    (should= (head l) (first (ht->list l)))
    (should= (tail l) (rest (ht->list l))))

  (it "should satisfy 'htlist?'"
    (should (htlist? l)))

  (it "should satisfy 'list?' when converted"
    (should (-list? (ht->list l)))
    (should-not (htlist? (ht->list l))))))

(describe "using lvars"
  (it "lvars from different contexts are not equivalent"
      (should-not= (lvar "X" 123) (lvar "X" 124))
      (should= (lvar "X" 123) (lvar "X" 123)))
  (it "lvars with different names are never equivalent, regardless of context ids"
      (should-not= (lvar "X" 123) (lvar "Y" 123))
      (should-not= (lvar "X") (lvar "Y"))))

(run-specs)
