(ns mapl.lang.primitive-query-spec
  (:require [speclj.core :refer :all]
            [mapl.lang.primitive-query :refer :all]
            [mapl.lang.configuration :as cfg]
            [mapl.lang.term :refer [-atom lvar id]]))

(describe "subgoal magic"
          (let [s {(lvar "X") 0}
                s' {(lvar "Y") 99}
                c (->
                    {}
                    (cfg/add-ai (id 1000) (-atom "test" []) #{})
                    (cfg/add-pi (id 1001) [] #{} s' nil)
                    (cfg/add-subgoal (id 1002) (-atom "test" []) (id 1001))
                    (cfg/add-ai (id 1003) (-atom "test" []) #{cfg/goal}))]
            (it "should leave the substitution alone if i is not an id for atom instance with goal tag"
                (should= s (subgoal-magic c (id 1000) s)))

            (it "should merge substitution s with the substitution from the parent plan instance, when the id is for a subgoal"
                (should-be #(and (contains? % (lvar "X"))
                                 (contains? % (lvar "Y")))
                           (subgoal-magic c (id 1002) s)))

            (it "should leave the substitution alone if i is the id for a top-level goal"
                (should= s (subgoal-magic c (id 1003) s)))))


(run-specs)


